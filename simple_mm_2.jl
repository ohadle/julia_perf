using Distributions, DataFrames, GLM, MixedModels, StatsBase
using BenchmarkTools


n_groups = 1000
samples_per_group = 5000

#linear model
function create_data(samples_per_group, n_groups)
  G = Array{Int}(samples_per_group, n_groups)
  X = Array{Float64}(samples_per_group, n_groups)
  Y = Array{Float64}(samples_per_group, n_groups)

  for group_ind in 1:n_groups
    a_group = rand(Uniform(0, group_ind))
    for sample_ind in 1:samples_per_group
      G[sample_ind, group_ind] = group_ind
      X[sample_ind, group_ind] = rand(Uniform(0, 0.5))
      Y[sample_ind, group_ind] = X[sample_ind, group_ind] + a_group + rand(Normal(0, 1))
    end
  end

  d = DataFrame(Y=Y[:], X=X[:], G=G[:])
  return d
end

function print_model_stats(model)
  println("AIC: ", aic(model))
  println("BIC: ", bic(model))
  println("R²: ", r²(model))
  println("Adjusted R²: ", adjr2(model))
  println("Confidence Intervals (95%): ", confint(model, 0.95))
end

function print_model_stats(model::MixedModel; n_bootstrap = 500, ci_quantiles = [0.025 0.975])
  println("AIC: ", aic(model))
  println("BIC: ", bic(model))

  println("Bootstrapping with N=$n_bootstrap for parameter CIs")
  model_bootstrap = bootstrap(n_bootstrap, model)

  for colname in names(model_bootstrap)
    if colname != :obj
      ci_results = quantile(model_bootstrap[colname], ci_quantiles)
      println("CI for $colname ($ci_quantiles): $ci_results")
    end
  end
end

linear_data = create_data(n_groups, samples_per_group)

fit(LinearModel, @formula(Y ~ X), create_data(2, 2))
@time linear_m = fit(LinearModel, @formula(Y ~ X), linear_data)
println(linear_m)
print_model_stats(linear_m)

perf_linear_m = @benchmark fit(LinearModel, @formula(Y ~ X), linear_data)
println(perf_linear_m)

fit!(lmm(@formula(Y ~ X + (1 | G)), create_data(2, 2)))
@time linear_mm = fit!(lmm(@formula(Y ~ X + (1 | G)), linear_data))
println(linear_mm)
print_model_stats(linear_mm)

perf_linear_mm = @benchmark fit!(lmm(@formula(Y ~ X + (1 | G)), linear_data))
println(perf_linear_mm)
