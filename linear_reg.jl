using Distributions
using GLM
using DataFrames
using Gadfly
# using Plots
# plotlyjs()

N = 20

# univariate
d = Normal(0, 1)
x = rand(d, N)

d_ϵ = Normal(0, 0.2)
ϵ = rand(d_ϵ, size(x))

y = x + ϵ

plot(x=x, y=y)


#fit
#https://juliaeconomics.com/2014/06/15/introductory-example-ordinary-least-squares/
function OLSestimator(x, y)
    estimate = inv(x'*x)*(x'*y)
    return estimate
end

estimates = OLSestimator(x, y)

println(estimates)

#multivariate
d_mv = MvNormal(eye(3))
x_mv = [rand(d_mv, N)' ones(N)]
β = [1.0, 2.0, 3.0, 4.0]
y_mv = x_mv * β + ϵ

estimates_mv = OLSestimator(x_mv, y_mv)
println(estimates_mv)

#With GLM
data = DataFrame(X=x, Y=y)
OLS = glm(@formula(Y ~ X), data, Normal(), IdentityLink())
