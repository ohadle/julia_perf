
#linear model
random.hierarchical.data.lin = function(I,n) {
G=matrix(NA,n,I)  
w=matrix(NA,n,I)
z=matrix(NA,n,I)
for (i in 1:I) {
x=runif(n,0,0.5)
a=rep(runif(1,0,i),n)
eps=rnorm(n,0,1)
G[,i]=i
w[,i]=x
z[,i]=x+a+eps
}
w=as.vector(w)
z=as.vector(z)
G=as.vector(G)

return(data.frame(cbind(z,w,G)))
}

#I=number of groups
#n=number of subjects per group
data.lin=random.hierarchical.data.lin(1000,5000)
colnames(data.lin)=c("y","x","G")
#data.lin[1:200,]

library(lme4)

tstart=Sys.time()
lin.fit <- lm(y ~ x  , data = data.lin)
tend=Sys.time()
total_time=tend-tstart
total_time

summary(lin.fit)

tstart=Sys.time()
hir.fit <- lmer(y ~ x  + (1 | G), data = data.lin)
tend=Sys.time()
total_time=tend-tstart
total_time

summary(hir.fit)



##############################################################

#logistic model
random.hierarchical.data.log = function(I,n) {
  G=matrix(NA,n,I)  
  w=matrix(NA,n,I)
  z=matrix(NA,n,I)
  for (i in 1:I) {
    x=runif(n,0,1)
    a=rep(runif(1,-i,i),n)
    p=exp(x+a)/(1+exp(x+a))
    G[,i]=i
    w[,i]=x
    z[,i]=rbinom(n,1,p)
    
  }
  w=as.vector(w)
  z=as.vector(z)
  G=as.vector(G)
  
  return(data.frame(cbind(z,w,G)))
}

#I=number of groups
#n=number of subjects per group
data.log=random.hierarchical.data.log(100,50)
colnames(data.log)=c("y","x","G")
#data.log

library(lme4)
lin.fit <- glm(y ~ x  ,family = binomial, data = data.log)
summary(lin.fit)

tstart=Sys.time()
hir.fit <- glmer(y ~ x  + (1 | G),family = binomial, data = data.log)
summary(hir.fit)
tend=Sys.time()
total_time=tend-tstart
total_time


